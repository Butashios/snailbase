function addLogEntry() {
    // Get the values from the form
    const logDate = document.getElementById("logDate").value;
    const logEntryText = document.getElementById("logEntry").value;

    // Create a new log entry div
    const logEntryDiv = document.createElement("div");
    logEntryDiv.className = "log-entry";

    // Create and append the date and log entry elements
    const dateElement = document.createElement("p");
    dateElement.textContent = `Date: ${logDate}`;
    logEntryDiv.appendChild(dateElement);

    const logTextElement = document.createElement("p");
    logTextElement.textContent = `Log Entry: ${logEntryText}`;
    logEntryDiv.appendChild(logTextElement);

    // Append the log entry div to the log entries container
    const logEntriesContainer = document.getElementById("logEntries");
    logEntriesContainer.appendChild(logEntryDiv);

    // Clear the form fields
    document.getElementById("logDate").value = "";
    document.getElementById("logEntry").value = "";
}
